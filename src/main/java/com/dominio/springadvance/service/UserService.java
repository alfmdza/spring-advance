package com.dominio.springadvance.service;

import com.dominio.springadvance.domain.User;
import com.dominio.springadvance.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public User createUser(User user){

        return userRepository.save(user);
    }

    public List<User> getAllUsers(){

        return (List<User>) userRepository.findAll();
    }

    public void deleteUser(User user){

        userRepository.delete(user);
    }

    public void deleteUserById(Long id) {
        userRepository.deleteById(id);
    }
    public Optional<User> findUserById(long id){
        
        return userRepository.findById(id);
        
    }



    public User putUserById(User user){

        return userRepository.save(user);

    }

    public User patchUserById(User user){

        return userRepository.save(user);

    }


}
