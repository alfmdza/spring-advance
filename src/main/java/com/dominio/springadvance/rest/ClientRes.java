package com.dominio.springadvance.rest;

import com.dominio.springadvance.domain.Client;
import com.dominio.springadvance.domain.User;
import com.dominio.springadvance.repository.ClientRepository;
import com.dominio.springadvance.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/clients")
public class ClientRes {

    @Autowired
    private ClientService clientService;

    @PostMapping
    private ResponseEntity<Client> saveClient(@RequestBody Client client){
        Client temporal = clientService.createClient(client);

        try {

            return ResponseEntity.created(new URI("/api/client" + temporal.getId())).body(temporal);

        }catch (Exception e){

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

        }

    }

    @GetMapping
    private ResponseEntity<List<Client>> showAllClients(){

        return ResponseEntity.ok(clientService.getAllClients());
    }

    @GetMapping(value = "/{id}")
    private ResponseEntity<Optional<Client>> showClientsById(@PathVariable("id") Long id){
        return ResponseEntity.ok(clientService.findClientById(id));

    }

    @DeleteMapping(value = "/delete")
    private ResponseEntity<String> deleteClient(@RequestBody Client client){
        clientService.deleteClient(client);
        return ResponseEntity.ok("resource delete"+client.getId());

    }

    @DeleteMapping(value = "/delete/{id}")
    private ResponseEntity<String> deleteClientById(@PathVariable("id") Long id){
        clientService.deleteClientById(id);
        return ResponseEntity.ok("resource delete");

    }


    @PutMapping(value = "/{id}")
    private ResponseEntity<Client> partialUpdateClient(@PathVariable("id") Long id,@Validated @RequestBody Client updates){
        Client client = clientService.findClientById(id).get();
        client.setName(updates.getName());
        client.setUser(updates.getUser());
        client.setAddresses(updates.getAddresses());
        client.setProducts(updates.getProducts());

        final Client updatedClient = clientService.putClientById(client);
        return ResponseEntity.ok(updatedClient);

    }

    @PatchMapping("/{id}/{name}")
    public ResponseEntity<Client> updateClientPartially(@PathVariable Long id, @PathVariable String name) {
        try {
            Client client = clientService.findClientById(id).get();
            client.setName(name);
            return new ResponseEntity<>(clientService.patchClientById(client), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
