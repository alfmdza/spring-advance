package com.dominio.springadvance.repository;

import com.dominio.springadvance.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

}
