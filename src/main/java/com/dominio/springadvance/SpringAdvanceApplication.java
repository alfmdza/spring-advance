package com.dominio.springadvance;

import com.dominio.springadvance.domain.*;
import com.dominio.springadvance.repository.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Set;

@SpringBootApplication
public class SpringAdvanceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringAdvanceApplication.class, args);
	}

	@Bean
	CommandLineRunner commandLineRunner(
			UserRepository userRepository,
			ClientRepository clientRepository,
			AddressRepository addressRepository,
			ProductRepository productRepository
	){
		return args ->{
			User user1 = userRepository.save(new User(null, "alf", "1234" ));

			Client client1 = clientRepository.save(new Client(null, "Alf Mdza", user1, null, null));

			//Addresses
			Address address1 = addressRepository.save(new Address(null, "Av. San Martin", "0548", client1));
			Address address2 = addressRepository.save(new Address(null, "Calle 1", "2530", client1));
			Address address3 = addressRepository.save(new Address(null, "Av. Alemania", "1234", client1));
			Address address4 = addressRepository.save(new Address(null, "Calle Norte", "125", client1));

			client1.setAddresses(Set.of(address1,address2,address3,address4));

			//Products
			Product p1 = productRepository.save(new Product(null, "MacBook Pro 16 GB", 2500.00 ));
			Product p2 = productRepository.save(new Product(null, "Iphone 13 Pro", 1300.00 ));
			Product p3 = productRepository.save(new Product(null, "Magic Mouse", 150.00 ));
			Product p4 = productRepository.save(new Product(null, "Hub USB", 70.00 ));

			client1.setProducts(Set.of(p1,p2,p3,p4));
			Client alf = clientRepository.save(client1);
			System.out.println("Client name: "+alf.getName());
			System.out.println("User name: "+alf.getUser().getUsername());
			alf.getAddresses().stream().forEach(
					a-> System.out.println("Address: "+a.getStreet()+ " " + a.getNumber())
			);
			alf.getProducts().stream().forEach(
					p-> System.out.println("Products: "+p.getName()+ " - Value: " + p.getPrice())
			);
		};

	}
}
