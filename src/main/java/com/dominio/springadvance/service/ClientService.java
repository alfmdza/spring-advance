package com.dominio.springadvance.service;

import com.dominio.springadvance.domain.Client;
import com.dominio.springadvance.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClientService {
    @Autowired
    private ClientRepository clientRepository;

    public Client createClient(Client client){

        return clientRepository.save(client);
    }

    public List<Client> getAllClients(){

        return (List<Client>) clientRepository.findAll();
    }

    public void deleteClient(Client client){

        clientRepository.delete(client);
    }

    public void deleteClientById(Long id) {
        clientRepository.deleteById(id);
    }
    public Optional<Client> findClientById(long id){

        return clientRepository.findById(id);

    }



    public Client putClientById(Client client){

        return clientRepository.save(client);

    }

    public Client patchClientById(Client client){

        return clientRepository.save(client);

    }
}
