package com.dominio.springadvance.rest;

import com.dominio.springadvance.domain.User;
import com.dominio.springadvance.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserRes {

    @Autowired
    private UserService userService;
    @PostMapping
    private ResponseEntity<User> saveUser(@RequestBody User user){
        User temporal = userService.createUser(user);

        try {
            return ResponseEntity.created(new URI("/api/user" + temporal.getId())).body(temporal);

        }catch (Exception e){

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

        }

    }

    @GetMapping
    private ResponseEntity<List<User>> showAllUsers(){

        return ResponseEntity.ok(userService.getAllUsers());
    }

    @GetMapping(value = "/{id}")
    private ResponseEntity<Optional<User>> showUsersById(@PathVariable("id") Long id){
        return ResponseEntity.ok(userService.findUserById(id));

    }

    @DeleteMapping(value = "/delete")
    private ResponseEntity<String> deleteUser(@RequestBody User user){
        userService.deleteUser(user);
        return ResponseEntity.ok("resource delete"+user.getId());

    }

    @DeleteMapping(value = "/delete/{id}")
    private ResponseEntity<String> deleteUserById(@PathVariable("id") Long id){
        userService.deleteUserById(id);
        return ResponseEntity.ok("resource delete");

    }


    @PutMapping(value = "/{id}")
    private ResponseEntity<User> partialUpdateUser(@PathVariable("id") Long id,@Validated @RequestBody User updates){
        User user = userService.findUserById(id).get();
        user.setUsername(updates.getUsername());
        user.setPassword(updates.getPassword());
        final User updatedUser = userService.putUserById(user);
        return ResponseEntity.ok(updatedUser);

    }

    @PatchMapping("/{id}/{password}")
    public ResponseEntity<User> updateUserPartially(@PathVariable Long id, @PathVariable String password) {
        try {
            User user = userService.findUserById(id).get();
            user.setPassword(password);
            return new ResponseEntity<>(userService.patchUserById(user), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



}
